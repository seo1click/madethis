<?php

function booking_component($post_id) {
    $tmp = get_page_template_slug($post_id);
    if('page-dev-new-bookpacks-cro.php' != $tmp && 'page-dev-new-bookpacks-cro-new.php' != $tmp && 'page-homepage-2018.php' != $tmp) {
        wp_enqueue_style('materialize-clockpicker-css', get_template_directory_uri() . '-child/booking/vendors/materialize-clockpicker/materialize.clockpicker.css');
        wp_enqueue_style('booking-icons-css', get_template_directory_uri() . '-child/booking/icons/css/maidthis.css');
        wp_enqueue_style('materialize-css', get_template_directory_uri() . '-child/booking/vendors/materialize/materialize.css', array('materialize-clockpicker-css'));
        wp_enqueue_style('booking-css', get_template_directory_uri() . '-child/booking/style.css', array('materialize-css'));
        //
        wp_enqueue_style('angular-css1', get_template_directory_uri() . '-child/booking/angular-tooltip/angular-tooltips.css', array('materialize-css'));
        //
        wp_enqueue_style('angular-css1', get_template_directory_uri() . '-child/booking/angular-tooltip.css', array('materialize-css'));
        //

        wp_enqueue_script('stripe', 'https://js.stripe.com/v2/', array('jquery'));
        wp_enqueue_script('materialize-js', get_template_directory_uri() . '-child/booking/vendors/materialize/materialize.min.js', array('jquery'));
        wp_enqueue_script('materialize-clockpicker-js', get_template_directory_uri() . '-child/booking/vendors/materialize-clockpicker/materialize.clockpicker.js', array('jquery'));
        wp_enqueue_script('angularjs', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.js', array('jquery'));

        wp_enqueue_script('angular-materialize', 'https://cdnjs.cloudflare.com/ajax/libs/angular-materialize/0.2.2/angular-materialize.min.js', array('angularjs'));
        wp_enqueue_script('angular-payments', get_template_directory_uri() . '-child/booking/vendors/angular-payments/angular-payments.min.js', array('angularjs'));
        wp_enqueue_script('angular-loggly', get_template_directory_uri() . '-child/booking/vendors/angular-loggly-logger.js', array('angularjs'));

        wp_enqueue_script('booking-component', get_template_directory_uri() . '-child/booking/booking.component.js', array('angularjs'));
        wp_enqueue_script('bookingForm-component', get_template_directory_uri() . '-child/booking/bookingForm.component.js', array('booking-component'));
        wp_enqueue_script('bookingProgress-component', get_template_directory_uri() . '-child/booking/bookingProgress.component.js', array('booking-component'));
        wp_enqueue_script('bookingSidebar-component', get_template_directory_uri() . '-child/booking/bookingSidebar.component.js', array('booking-component'));
        wp_enqueue_script('bookingExtras-component', get_template_directory_uri() . '-child/booking/bookingExtras.component.js', array('booking-component'));
        wp_enqueue_script('bookingRequest-service', get_template_directory_uri() . '-child/booking/bookingRequest.service.js', array('bookingForm-component'));

        //
        wp_enqueue_script('angular-js1', get_template_directory_uri() . '-child/booking/angular-tooltip/angular-tooltips.min.js' ,array('bookingForm-component'));
        wp_enqueue_script('index-js1', get_template_directory_uri() . '-child/booking/angular-tooltip/index.js',array('bookingForm-component'));

        // //

        //   wp_enqueue_script('angular-js2', get_template_directory_uri() . '-child/booking/angular-tooltips.js' ,array('bookingForm-component'));
        // wp_enqueue_script('index-js2', get_template_directory_uri() . '-child/booking/index.js',array('bookingForm-component'));

        // //
        wp_enqueue_script('bookingApi-service', get_template_directory_uri() . '-child/booking/bookingApi.service.js', array('bookingRequest-service'));
    } else {
        wp_enqueue_style('materialize-clockpicker-css', get_template_directory_uri() . '-child/booking-new/vendors/materialize-clockpicker/materialize.clockpicker.css');
        wp_enqueue_style('booking-icons-css', get_template_directory_uri() . '-child/booking-new/icons/css/maidthis.css');
        wp_enqueue_style('materialize-css', get_template_directory_uri() . '-child/booking-new/vendors/materialize/materialize.css', array('materialize-clockpicker-css'));
        wp_enqueue_style('booking-css', get_template_directory_uri() . '-child/booking-new/style.css', array('materialize-css'));
        //
        wp_enqueue_style('angular-css1', get_template_directory_uri() . '-child/booking-new/angular-tooltip/angular-tooltips.css', array('materialize-css'));
        //
        wp_enqueue_style('angular-css1', get_template_directory_uri() . '-child/booking-new/angular-tooltip.css', array('materialize-css'));
        //

        wp_enqueue_script('stripe', 'https://js.stripe.com/v2/', array('jquery'));
        wp_enqueue_script('materialize-js', get_template_directory_uri() . '-child/booking-new/vendors/materialize/materialize.min.js', array('jquery'));
        wp_enqueue_script('materialize-clockpicker-js', get_template_directory_uri() . '-child/booking-new/vendors/materialize-clockpicker/materialize.clockpicker.js', array('jquery'));
        wp_enqueue_script('angularjs', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.js', array('jquery'));

        wp_enqueue_script('angular-materialize', 'https://cdnjs.cloudflare.com/ajax/libs/angular-materialize/0.2.2/angular-materialize.min.js', array('angularjs'));
        wp_enqueue_script('angular-payments', get_template_directory_uri() . '-child/booking-new/vendors/angular-payments/angular-payments.min.js', array('angularjs'));
        wp_enqueue_script('angular-loggly', get_template_directory_uri() . '-child/booking-new/vendors/angular-loggly-logger.js', array('angularjs'));

        wp_enqueue_script('booking-component', get_template_directory_uri() . '-child/booking-new/booking.component.js', array('angularjs'));
        wp_enqueue_script('bookingForm-component', get_template_directory_uri() . '-child/booking-new/bookingForm.component.js', array('booking-component'));
        wp_enqueue_script('bookingProgress-component', get_template_directory_uri() . '-child/booking-new/bookingProgress.component.js', array('booking-component'));
        wp_enqueue_script('bookingSidebar-component', get_template_directory_uri() . '-child/booking-new/bookingSidebar.component.js', array('booking-component'));
        wp_enqueue_script('bookingExtras-component', get_template_directory_uri() . '-child/booking-new/bookingExtras.component.js', array('booking-component'));
        wp_enqueue_script('bookingRequest-service', get_template_directory_uri() . '-child/booking-new/bookingRequest.service.js', array('bookingForm-component'));

        //
        wp_enqueue_script('angular-js1', get_template_directory_uri() . '-child/booking-new/angular-tooltip/angular-tooltips.min.js' ,array('bookingForm-component'));
        wp_enqueue_script('index-js1', get_template_directory_uri() . '-child/booking-new/angular-tooltip/index.js',array('bookingForm-component'));

        // //

        //   wp_enqueue_script('angular-js2', get_template_directory_uri() . '-child/booking/angular-tooltips.js' ,array('bookingForm-component'));
        // wp_enqueue_script('index-js2', get_template_directory_uri() . '-child/booking/index.js',array('bookingForm-component'));

        // //
        wp_enqueue_script('bookingApi-service', get_template_directory_uri() . '-child/booking-new/bookingApi.service.js', array('bookingRequest-service'));
    }

}

add_action('wp_enqueue_scripts', 'booking_component');

function additional_widgets_init() {

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widgets' ),
        'id'            => 'footer-widget',
        'description'   => esc_html__( 'Add widgets here.' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'additional_widgets_init' );


?>