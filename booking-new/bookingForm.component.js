(function () {
    'use strict';

    angular
        .module('booking-new')
        .component('bookingForm', {
            templateUrl: 'booking-new/bookingForm.template.html',
            controller: BookingFormController,
            bindings: {
                currentStep: '<',
                lists: '<',
                formStep: '=',
                steps: '=',
                airnbn: '=',
                nextStep: '&'
            }
        });

    BookingFormController.inject = ['$scope', '$timeout', 'BookingApi'];
    function BookingFormController($scope, $timeout, BookingApi) {
        var $ctrl = this;

        $ctrl.step_4_chooseTime = true;
        $ctrl.step_3_hourlyLists = {};
        $ctrl.step_3_greaterService;
        $ctrl.step_3_laundryLoadQty;
        $ctrl.step_4_mintDate;
        $ctrl.step_4_available_spots;
        $ctrl.step_4_frequencyTitles;
        $ctrl.step_4_popularFrequencyId;
        $ctrl.step_4_openFreqModal = false;
        $ctrl.step_4_openTurnoverModal = false;
        $ctrl.step_4_oneTimeFreqId = BookingApi.oneTimeFrequencyId;
        $ctrl.step_4_popularFreqId = BookingApi.popularFrequencyId;
        $ctrl.step_6_noLaundryDiscountUsed = false;
        $ctrl.noLaundryDiscountCode = BookingApi.noLaundryDiscountCode;

        $ctrl.step_3_serviceSelection = serviceSelection;
        $ctrl.step_3_setService = setService;
        $ctrl.step_4_setAvailableSpots = setAvailableSpots;
        $ctrl.step_4_setFrequency = setFrequency;
        $ctrl.step_4_setPopularFrequency = setPopularFrequency;
        $ctrl.step_4_setTurnoverVolumen = setTurnoversVolumen;
        $ctrl.step_5_setSourceOptions = step_5_setSourceOptions;
        $ctrl.step_5_setPetOption = step_5_setPetOption;
        $ctrl.setSomeoneHome = setSomeoneHome;
        $ctrl.step_2_setSourceOptions = step_2_setSourceOptions;
        $ctrl.step_6_getBookingEstimate = getBookingEstimate;

        ////////////////

        $ctrl.$onInit = function () {
            $ctrl.icons = BookingApi.extrasIcons;

            /** Default State */
            $ctrl.steps[2].state = BookingApi.defaultState;

            /** Greater Service */
            $ctrl.step_3_greaterService = BookingApi.greaterServiceId;
            $ctrl.step_3_laundryLoadQty = 1;

            /** Min Date for Datepicker */
            $ctrl.step_4_mintDate = getMintDate();

            /** Enums for One Time Frequency */
            $ctrl.step_4_oneTimeFrequencyId = BookingApi.oneTimeFrequencyId;

            /** Enums for Popular Frequency */
            $ctrl.step_4_popularFrequencyId = BookingApi.popularFrequencyId;

            /** Default Frequency */
            $ctrl.steps[4].frequency = $ctrl.lists.frequencies.filter(function (frequency) {
                return frequency.id === $ctrl.step_4_popularFrequencyId;
            })[0];
            $ctrl.steps[5].someoneNotHome = false;
        };
        $ctrl.$onChanges = function (changesObj) {
            if (!changesObj.currentStep.currentValue) return;

            if (!$scope["f_step_" + changesObj.currentStep.currentValue]) return;

            $ctrl.formStep = $scope["f_step_" + changesObj.currentStep.currentValue];

            if (changesObj.currentStep.currentValue == 12) {
                getBookingEstimate();
            }
        };
        $ctrl.$postLink = function () {
            // Hack to Initial valid state (after render)
            $timeout(function () {
                $ctrl.formStep = $scope["f_step_" + $ctrl.currentStep];
            });
        };
        $ctrl.$onDestory = function () { };

        ////////////////

        function serviceSelection() {
            clearServiceSelection();

            $ctrl.step_3_laundryLoadQty = BookingApi.laundryQtyByService[$ctrl.steps[3].service.id];
        }

        function setService(service) {
            clearServiceSelection();
            $ctrl.steps[3].service = service;

            $ctrl.step_3_laundryLoadQty = BookingApi.laundryQtyByService[$ctrl.steps[3].service.id];
            $ctrl.nextStep();
        }

        function step_5_setSourceOptions(source) {
            $ctrl.steps[5].source = source.id;
            $ctrl.nextStep();
        }

        function step_2_setSourceOptions() {
            $ctrl.steps[3].maids = $ctrl.steps[3].service.maidsList[0];
            $ctrl.nextStep();
        }

        function step_5_setPetOption(hasPet) {
            $ctrl.steps[5].has_pets = hasPet.id;
            if (hasPet.id === 24) {
                $ctrl.nextStep();
            }
        }

        function setSomeoneHome(someone, ind) {
            if (ind) {
                $ctrl.steps[5].someone_home = someone;
                $ctrl.steps[5].someoneNotHome = false;
                $ctrl.nextStep();
            } else {
                $ctrl.steps[5].someone_home = '';
                $ctrl.steps[5].someoneNotHome = true;
            }
        }

        function clearServiceSelection() {
            $ctrl.steps[3].maids = null;
            $ctrl.steps[3].hours = null;
        }

        function getMintDate() {
            var currentDate = new Date();
            var days = 1;
            return (new Date(currentDate.getTime() + (1000 * 60 * 60 * 24 * days))).toISOString();
        }

        function setAvailableSpots() {
            $ctrl.steps[4].spot = null;
            $ctrl.step_4_chooseTime = true;
            $ctrl.step_4_available_spots = [];

            if (!$ctrl.steps[4].date) return;

            BookingApi.getAvailableSpots($ctrl.steps[4].date).then(function (spots) {
                $ctrl.step_4_available_spots = spots;
                $ctrl.step_4_chooseTime = false;
            }).catch(function (err) {
                Materialize.toast("Server Error. Please try again later.", 10000, 'red');
                console.log(err);
            });
        }


        function setFrequency(frequency) {
            $ctrl.steps[4].frequency = frequency;
            if (frequency.id === $ctrl.step_4_oneTimeFreqId) {
                $ctrl.step_4_openFreqModal = true;
            }
            if (frequency.id !== 1) {
                $ctrl.nextStep();
            }
        }

        function setPopularFrequency() {
            $ctrl.steps[4].frequency = $ctrl.lists.frequencies.filter(function (freq) {
                return freq.id === $ctrl.step_4_popularFreqId;
            })[0];
        }

        function setTurnoversVolumen(turnoverValue) {
            $ctrl.steps[4].turnovers = turnoverValue;

            // $ctrl.step_4_openTurnoverModal_1 = $ctrl.step_4_openTurnoverModal_2 = false;

            if (turnoverValue == BookingApi.discountTurnoverValue_1) {
                $ctrl.step_4_openTurnoverModal_1 = true;
            } else if (turnoverValue == BookingApi.discountTurnoverValue_2) {
                $ctrl.step_4_openTurnoverModal_2 = true;
            }
        }

        function getBookingEstimate() {
            $scope.$emit('booking-form: get-estimate', $ctrl.steps);

            $ctrl.step_6_noLaundryDiscountUsed = BookingApi.noLaundryDiscount;
        }

    }
})();