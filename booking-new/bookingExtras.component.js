(function () {
    'use strict';

    angular
        .module('booking-new')
        .component('bookingExtras', {
            templateUrl: 'booking-new/bookingExtras.template.html',
            controller: ControllerController,
            bindings: {
                extras: '<',
                selectedExtras: '=',
                airnbn: '='
            }
        });

    ControllerController.inject = ['BookingApi'];
    function ControllerController(BookingApi) {
        var selected = [];
        var $ctrl = this;

        $ctrl.icons;
        $ctrl.extrasWithQuantityIds = {};
        $ctrl.quantities = {};

        $ctrl.toggleSelect = toggleSelect;
        $ctrl.isSelected = isSelected;
        $ctrl.setQuantity = setQuantity;
        $ctrl.isQuantity = isQuantity;

        ////////////////

        $ctrl.$onInit = function () {
            /** Enums for Extras */
            $ctrl.icons = BookingApi.extrasIcons;

            BookingApi.extrasWithQuantityIds.map(function (extraWQty) {
                $ctrl.extrasWithQuantityIds[extraWQty.id] = extraWQty;
                $ctrl.quantities[extraWQty.id] = null;
            })

            $ctrl.extrasAirnbnNoLaundryId = BookingApi.extrasAirnbnNoLaundryId;
        };

        $ctrl.$onChanges = function (changesObj) {
            if (changesObj.extras) {
                selected = [];
            }
        };
        $ctrl.$onDestory = function () { };

        ////////////////

        function toggleSelect(extraId) {
            if (isSelected(extraId)) {
                selected = selected.filter(function (_extra) {
                    return _extra.id !== extraId;
                });
            } else {
                selected.push({
                    id: extraId,
                    quantity: 1
                });
            }

            if (extraId === $ctrl.extrasAirnbnNoLaundryId) {
                BookingApi.noLaundryDiscount = !BookingApi.noLaundryDiscount;
            }

            $ctrl.selectedExtras = angular.copy(selected);
        }

        function isSelected(extraId) {
            var extraSelected = selected.filter(function (_extra) {
                return _extra.id === extraId;
            });

            return extraSelected.length > 0;
        }

        function setQuantity(extraId) {
            selected = selected.filter(function (_extra) {
                return _extra.id !== extraId;
            });

            if ($ctrl.quantities[extraId]) {
                selected.push({
                    id: extraId,
                    quantity: $ctrl.quantities[extraId]
                });
            }

            $ctrl.selectedExtras = angular.copy(selected);
        }

        function isQuantity(extraId) {
            return $ctrl.extrasWithQuantityIds.hasOwnProperty(extraId);
        }

    }
})();