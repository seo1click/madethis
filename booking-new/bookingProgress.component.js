(function () {
    'use strict';

    angular
        .module('booking-new')
        .component('bookingProgress', {
            templateUrl: 'booking-new/bookingProgress.template.html',
            controller: BookingProgressController,
            bindings: {
                currentStep: '<',
                totalSteps: '<'
            }
        });

    BookingProgressController.inject = [];
    function BookingProgressController() {
        var $ctrl = this;

        $ctrl.currentProgress;
        $ctrl.currentProgressStyle;

        ////////////////

        $ctrl.$onInit = function () { };
        $ctrl.$onChanges = function (changesObj) {
            setCurrentProgress();
        };
        $ctrl.$onDestory = function () { };

        ////////////////

        function setCurrentProgress() {
            $ctrl.currentProgress = Math.round((100 / $ctrl.totalSteps) * $ctrl.currentStep);
            $ctrl.currentProgressStyle = { 'width': $ctrl.currentProgress + "%" };

        }
    }
})();