(function () {
    'use strict';

    angular
        .module('booking-new')
        .service('BookingRequestService', Service);

    Service.inject = [];
    function Service() {
        this.getBookingRequest = getBookingRequest;
        this.getBookingEstimateRequest = getBookingEstimateRequest;

        ////////////////

        function getBookingEstimateRequest(bookingInfo, airnbnRequest, stripeToken, airbnbProps) {
            //Solves empty extras list
            bookingInfo[2].completeAddress = bookingInfo[2].address;
            if(bookingInfo[2].address_2) {
                bookingInfo[2].completeAddress += ", " + bookingInfo[2].address_2;
            }
            bookingInfo[3].extras = bookingInfo[3].extras || [];
            bookingInfo[3].maids = bookingInfo[3].maids || 0;
            bookingInfo[3].minutes = bookingInfo[3].hours * 60 || 0;


            var request = {
                    "service_date": bookingInfo[4].spot.format_date+":00",
                    "frequency_id": bookingInfo[4].frequency.id,
            }

            request.tip = (bookingInfo[6].tip) ? parseFloat(bookingInfo[6].tip) : 0;

            request = (airnbnRequest) ? setAirnbnProperties(request, bookingInfo, airbnbProps) : setHomeProperties(request, bookingInfo);
            console.log('Request', request);
            return request;
        }

        function getBookingRequest(bookingInfo, airnbnRequest, customFieldNames, customFieldsIds, stripeToken, airbnbProps) {
            //Solves empty extras list
            //console.log("bookingInfo", bookingInfo);
            bookingInfo[2].completeAddress = bookingInfo[2].address;
            if(bookingInfo[2].address_2) {
                bookingInfo[2].completeAddress += ", " + bookingInfo[2].address_2;
            }
            bookingInfo[3].extras = bookingInfo[3].extras || [];
            bookingInfo[3].maids = bookingInfo[3].maids || 0;
            bookingInfo[3].minutes = bookingInfo[3].hours * 60 || 0;


            // Pack customer comments as single note
            var customer_notes = "";
            var additional_comments = bookingInfo[5].comments || "No additional comments";
            customer_notes += "Cleaners Parking: \n - " + bookingInfo[5].cleaners_parking + " \n \n";
            customer_notes += "Will someone be there? \n - " + bookingInfo[5].someone_home + " \n \n";
            customer_notes += "Focus Areas: \n - " + bookingInfo[5].focus_areas + " \n \n";
            customer_notes += "Additional Comments: \n - " + additional_comments + " \n \n";


            var request = {
                
                    "service_date": bookingInfo[4].spot.format_date+":00",
                    //"flexibility": bookingInfo[4].spot.flexibility,
                    "arrival_window": 60,
                    "address": bookingInfo[2].completeAddress,
                    "city": bookingInfo[2].city,
                    "state": bookingInfo[2].state, // Leaving static for now
                    "zip": bookingInfo[2].zip_code,
                    "phone": bookingInfo[1].phone,
                    "customer_notes": customer_notes,
                    "sms_notifications": false,
                    "frequency_id": bookingInfo[4].frequency.id,
                    "payment_method": "stripe",
                    "discount_code": bookingInfo[6].discount_code || null,
                    "tip": (bookingInfo[6].tip) ? parseFloat(bookingInfo[6].tip) : 0,
                
                "user": {
                    "email": bookingInfo[1].email,
                    "first_name": bookingInfo[1].first_name,
                    "last_name": bookingInfo[1].last_name
                }
            }

            console.log('Request Mid', request);

            if (stripeToken) {
                request.stripe_token =  stripeToken ;
            }

            request = (airnbnRequest) ? setAirnbnProperties(request, bookingInfo, customFieldNames, airbnbProps) : setHomeProperties(request, bookingInfo, customFieldNames, customFieldsIds);
            console.log('Request 2', request);
            return request;
        }

        function setHomeProperties(request, bookingInfo, customFieldNames = false, customFieldsIds = false) {
            request["frequency_id"] = bookingInfo[4].frequency.id;

            // If there's an hourly service
            if(bookingInfo[3].maids != "" && bookingInfo[3].minutes != ""){

                request["services"] = [
                    {
                        "id": bookingInfo[3].service.id,
                        "extras": bookingInfo[3].extras.map(function (_extra) {
                            return {
                                "id": _extra.id,
                                "quantity": _extra.quantity
                            }
                        }),
                        "hourly": {
                            "quantity": bookingInfo[3].maids,   // ex "service_maids": bookingInfo[3].maids,
                            "minutes": bookingInfo[3].minutes,  // ex"service_minutes": bookingInfo[3].minutes
                        },
                        "pricing_parameters": []
                    }
                ];

            }else{
                request["services"] = [
                {
                    "id": bookingInfo[3].service.id,
                    "extras": bookingInfo[3].extras.map(function (_extra) {
                        return {
                            "id": _extra.id,
                            "quantity": _extra.quantity
                        }
                    }),
                    "pricing_parameters": []
                }
            ];    
            }

            

            if(customFieldNames){

            // request.custom_fields = {} // Original    

            request.custom_fields = []
            
            if(bookingInfo[5].source != null){
                //request.custom_fields.push({ id: customFieldsIds[0], values: [{ id: bookingInfo[5].source }] } );    
                request.custom_fields.push({ id: customFieldsIds[5], values: [{ id: bookingInfo[5].source }] } );    
            }

            if(bookingInfo[5].has_pets != null){
                // request.custom_fields.push({ id: customFieldsIds[1], values: [{ id: bookingInfo[5].has_pets }] } );    
                request.custom_fields.push({ id: customFieldsIds[6], values: [{ id: bookingInfo[5].has_pets }] } );    
            }

            if(bookingInfo[5].pet_type != null){
                // request.custom_fields.push({ id: customFieldsIds[2], value: bookingInfo[5].pet_type } );    
                request.custom_fields.push({ id: customFieldsIds[7], value: bookingInfo[5].pet_type } );    
            }

            /* Original
            request.custom_fields[customFieldNames['source']] = { values: bookingInfo[5].source };
            request.custom_fields[customFieldNames['hasPets']] = { values: bookingInfo[5].has_pets };
            request.custom_fields[customFieldNames['petType']] = bookingInfo[5].pet_type;
              */  
            }

            return request;
        }

        function setAirnbnProperties(request, bookingInfo, customFieldNames = false, airbnbProps) {
            request.user.last_name = request.user.last_name + " - Airbnb";

            request["frequency_id"] = 1;

            request["services"] = [
                {
                    "id": bookingInfo[3].service.id,
                    "extras": bookingInfo[3].extras.map(function (_extra) {
                        return {
                            "id": _extra.id,
                            "quantity": _extra.quantity
                        }
                    }),
                    "hourly": {
                        "quantity": bookingInfo[3].maids,   // ex "service_maids": bookingInfo[3].maids,
                        "minutes": bookingInfo[3].minutes,  // ex"service_minutes": bookingInfo[3].minutes
                    },
                    "pricing_parameters": []
                }
            ];

            if (airbnbProps.noLaundryDiscount) {
                request.discount_code = airbnbProps.noLaundryDiscountCode;
            }

if(customFieldNames){

            request.custom_fields = []
            
            if(bookingInfo[2].unit_type != null){
                // request.custom_fields.push({ id: customFieldsIds[5], values: [{ id: bookingInfo[2].unit_type }] } );    
                request.custom_fields.push({ id: customFieldsIds[0], values: [{ id: bookingInfo[2].unit_type }] } );    
            }

            if(bookingInfo[2].airnbn_ical != null){
                // request.custom_fields.push({ id: customFieldsIds[6], value: bookingInfo[2].airnbn_ical } );    
                request.custom_fields.push({ id: customFieldsIds[1], value: bookingInfo[2].airnbn_ical } );    
            }

            if(bookingInfo[4].checkout_time != null){
                request.custom_fields.push({ id: customFieldsIds[3], values: [{ id: bookingInfo[4].checkout_time }] } );    
            }

            if(bookingInfo[4].checkin_time != null){
                request.custom_fields.push({ id: customFieldsIds[4], values: [{ id: bookingInfo[4].checkin_time }] } );    
            }

            if(bookingInfo[4].turnovers != null){
                // request.custom_fields.push({ id: customFieldsIds[7], values: [{ id: bookingInfo[4].turnovers }] } );    
                request.custom_fields.push({ id: customFieldsIds[2], values: [{ id: bookingInfo[4].turnovers }] } );    
            }
            
            
            

            /* Original
            request.custom_fields = {}
            request.custom_fields[customFieldNames['unit_type']] = { "values": bookingInfo[2].unit_type }
            request.custom_fields[customFieldNames['airnbn_ical']] = bookingInfo[2].airnbn_ical;
            request.custom_fields[customFieldNames['checkout_time']] = { "values": bookingInfo[4].checkout_time };
            request.custom_fields[customFieldNames['checkin_time']] = { "values": bookingInfo[4].checkin_time };
            request.custom_fields[customFieldNames['turnovers']] = { "values": bookingInfo[4].turnovers };
            */
}
            request['customer_comments'] = 'Entry Instructions: ' + bookingInfo[5].airnbn_comments_1 + ' \n'
                + 'Parking: ' + bookingInfo[5].airnbn_comments_2 + ' \n'
                + 'Laundry Instructions: ' + bookingInfo[5].airnbn_comments_3 + ' \n'
                + 'Spare linens: ' + bookingInfo[5].airnbn_comments_4 + ' \n'
                + 'Garbage: ' + bookingInfo[5].airnbn_comments_5 + ' \n'
                + 'Emergency Info: ' + bookingInfo[5].airnbn_comments_6 + ' \n'
                + 'Additional Notes: ' + bookingInfo[5].airnbn_comments_7 + ' \n';

            return request;
        }
    }
})();