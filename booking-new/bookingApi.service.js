(function () {
    'use strict';

    angular
        .module('booking-new')
        .service('BookingApi', BookingApi);

    BookingApi.inject = ['$http', '$q', '$filter', 'BookingRequestService'];
    function BookingApi($http, $q, $filter, BookingRequestService) {
        var service = this;

        /** Shared Properties */
        service.airnbnBooking = false; //Init this in BookingForm Component if needed
        service.noLaundryDiscount = false;
        service.residentialRedirect = "/thanks-for-booking";
        service.airnbnRedirect = "/turnover-thanks";

        /** General Enums */
        ///////////////////
        service.residentialSkipTime = {
            from: {
                hours: 10,
                minutes: 30
            },
            to: {
                hours: 15,
                minutes: 0
            }
        }
        service.noLaundryDiscountCode = "AIRBNB_NOLAUNDRY";

        /** LIVE Enums */
        ////////////////
        var customFieldsIds = [30, 254, 255, 259, 260, 257, 258, 256];
        var customFieldNames = {
            source: "source",
            hasPets: "drop_down_1474711135231",
            petType: "single_line_1474711168613",
            checkout_time: "drop_down:073ba7ea-6016-42e9-88da-ddfab3bcef9e",
            checkin_time: "drop_down:5ca96886-5ac2-48d3-bf8b-79798f528810",
            unit_type: "drop_down:e018608a-1e0e-440b-bd69-f82a7af93153",
            airnbn_ical: "single_line:e9fd89af-65a6-44a0-9ea2-79abc5f6104e",
            turnovers: "drop_down:4d771a59-ad4b-44ae-942c-f479dc03f3e7"
        }
        var stripeKey = "pk_live_jBJi8rIptpYPAercksB8I7UX";
        var apiConfig = {};
        var apiUrl = "https://maidthis.launch27.com/latest";
        var apiKey = "live_VTBtNULBMJerlYGA3uJ1";
        service.defaultState = "CA";
        service.extrasIcons = {
            27: 'nevera',
            18: 'spray',
            2: 'ventanas',
            24: 'caja',
            28: 'lavaplatos',
            12: 'spray',
            16: 'check',
            10: 'spray',
            25: 'caja',
            15: 'spray',
            20: 'ventanas',
            26: 'caja',
            30: 'spray',
            31: 'spray',
            32: 'spray',
            33: 'spray',
            37: 'caja',
            38: 'caja',
            39: 'lavadora',
            40: 'spray'

        };
        service.extrasWithQuantityIds = [
            {
                id: 12,
                min: 2
            }
        ];
        service.oneTimeFrequencyId = 1;
        service.popularFrequencyId = 8;

        service.greaterServiceId = null;
        service.extrasForAirnbnOnly = [29, 30, 31, 32, 33];
        service.extrasIncludedInAirnbn = [18, 27, 28, 29];
        service.extrasAirnbnNoLaundryId = 33;
        service.extrasAirbnbMappingNames = {
            24: "New Unit Cleaning",
            25: "New Unit Cleaning",
            26: "New Unit Cleaning"
        }
        service.laundryQtyByService = {
            20: 1,
            21: 1,
            22: 2,
            23: 3,
            24: 4,
            25: 4
        }
        service.discountTurnoverValue_1 = "option:757364c3-9d6d-499e-830d-48eae25df41c";
        service.discountTurnoverValue_2 = "option:9290166e-3289-47db-8739-673edcb1e0c4";
        
        /** SANDBOX Enums */
        ///////////////////
        /*var customFieldsIds = [245, 246, 247, 248, 249, 250, 251, 252];
        var customFieldNames = {
            source: "drop_down:bd99adac-cd2a-4493-a226-223c0487e19e",
            hasPets: "drop_down:e459d440-692d-450c-9ae5-576bd1542574",
            petType: "single_line:7ac456b1-88ab-4ced-9edd-a0d8a38b16fb",
            checkout_time: "drop_down:d4882270-22b4-4f16-8d80-957ab8f9d125",
            checkin_time: "drop_down:86f8cac5-f9b2-43db-ae6e-a00b2a533dae",
            unit_type: "drop_down:f33fc03e-01d3-4daa-a486-0db73b79a09c",
            airnbn_ical: "single_line:f4e5cc0e-ab10-446a-bd65-e379484633ad",
            turnovers: "drop_down:e0469b73-fe7b-4660-8a5f-5f16e8832262"
        }
        var stripeKey = "pk_test_RCC5U7OSotpn2YFuAwDJjxIK";
        var apiConfig = {};
        var apiUrl = "https://maidthis-sandbox.launch27.com/latest";
        var apiKey = "sandbox_NYxWWPOEGS7bbGykWgHC";
        service.defaultState = "CA";
        service.extrasIcons = {
            1: 'nevera',
            2: 'spray',
            3: 'caja',
            4: 'spray',
            5: 'spray',
            6: 'ventanas',
            7: 'caja',
            8: 'caja',
            9: 'lavaplatos',
            10: 'spray',
            11: 'check',
            12: 'spray',
            13: 'spray',
            14: 'spray',
            15: 'spray',
            16: 'spray',
            17: 'spray',
            18: 'spray'
        };
        service.extrasWithQuantityIds = [
            {
                id: 10,
                min: 2
            }
        ];
        service.oneTimeFrequencyId = 1;
        service.popularFrequencyId = 4;
        service.greaterServiceId = 4;
        service.extrasForAirnbnOnly = [12, 13, 14, 15, 18];
        service.extrasIncludedInAirnbn = [1, 9, 12, 14];
        service.extrasAirnbnNoLaundryId = 18;
        service.extrasAirbnbMappingNames = {
            7: "New Unit Cleaning",
            8: "New Unit Cleaning",
            3: "New Unit Cleaning"

        }
        service.laundryQtyByService = {
            1: 1,
            2: 1,
            5: 2,
            3: 3,
            6: 4,
            7: 4
        }
        service.discountTurnoverValue_1 = "option:b4b29c14-a7cf-4ce9-b31d-d2af03d81df4";
        service.discountTurnoverValue_2 = "option:68f351d0-968d-417d-acd7-e747dd47bc12";*/

        /** Stripe Global Config */
        //////////////////////////
        Stripe.setPublishableKey(stripeKey);

        /** API Methods */
        /////////////////
        service.getLists = getLists;
        service.getServices = getServices;
        service.getFrequencies = getFrequencies;
        service.getAvailableSpots = getAvailableSpots;
        service.getCustomFields = getCustomFields;
        service.getBookingEstimate = getBookingEstimate;
        service.createBooking = createBooking;

        ////////////////

        function getLists() {
            return $q.all({
                services: getServices(),
                frequencies: getFrequencies(),
                customFields: getCustomFields()
            });
        }

        function getServices() {
            return $http.get(apiUrl + '/booking/services', apiConfig)
                .then(function (results) {
                    //return results.data.services;
					var services = results.data;
					return services;
                })
                .then(function (services) {
                    services = services.map(function (srv) {
                        /** Hourly Service won't have Extras Excludes (for now) */
                        if (srv.hourly) {
                            return getHourlyExtraLists(srv);
                        }

                        /** Exclude Extras and Map Extra Namings */
                        if (service.airnbnBooking) {
                            return mapAirbnbExtraNames(excludeExtrasFromService(srv));
                        } else {
                            return excludeExtrasFromService(srv);
                        }
                    });
					//console.log("Services: ",services);
                    return services;
                });
        }

        function getHourlyExtraLists(srv) {
            var maidsList = [];
            var hoursList = [];

            // for (var i = srv.maids_minimum; i <= srv.maids_maximum; i++) {
            //     maidsList.push(i);
            // }

            // for (var j = srv.hours_minimum; j <= srv.hours_maximum; j = j + 0.5) {
            //     hoursList.push(j);
            // }

            for (var i = srv.hourly.quantity_minimum; i <= srv.hourly.quantity_maximum; i++) {
                maidsList.push(i);
            }

            for (var j = (srv.hourly.minutes_minimum/60); j <= (srv.hourly.minutes_maximum/60); j = j + 0.5) {
                hoursList.push(j);
            }


            srv.maidsList = maidsList;
            srv.hoursList = hoursList;
			//console.log("Srv: ",srv);
            return srv;
        }

        function excludeExtrasFromService(srv) {
            if (!service.airnbnBooking) {
                srv.extras = srv.extras.filter(function (extra) {
                    return service.extrasForAirnbnOnly.indexOf(extra.id) < 0;
                });
            } else {
                srv.extras = srv.extras.filter(function (extra) {
                    return service.extrasIncludedInAirnbn.indexOf(extra.id) < 0;
                });
            }

            return srv;
        }

        function mapAirbnbExtraNames(srv) {
            srv.extras = srv.extras.map(function (extra) {
                if (service.extrasAirbnbMappingNames[extra.id]) {
                    extra.name = service.extrasAirbnbMappingNames[extra.id] + " (+$" + extra.price + ")";
                }

                return extra;
            });

            return srv;
        }

        function getFrequencies() {
            return $http.get(apiUrl + '/booking/frequencies', apiConfig).then(function (results) {
				//console.log("FRQ: ",results.data);
                return results.data;
            })
        }

        function getAvailableSpots(date) {
            var sDate = angular.copy(date).split('/');
            var tDate = sDate[2] + '-' + sDate[0] + '-' + sDate[1];
			
			var spotsData = JSON.stringify({
				"date" : ""+tDate+"",
				"mode" : "new",
				"days" : 1
			});
			
            return $http.post(apiUrl + '/booking/spots', spotsData, apiConfig )
                .then(function (results) {
					// console.log('Here', results);
                    //return results.data.spots;
					var spots_data = results.data[0];
					//var service_date = results.data[0].date;
					//console.log("Spots: ",spots);
					//console.log("Service Date: ",service_date);
					return spots_data;
                })
                .then(function (spots_data) {
                    var newSpots = [];

                    var spots = spots_data.spots;
                    var service_date = spots_data.date;
	                
                    spots.forEach(function(spot) {
						//console.log('Single spot: ',spot);
                        var flex = spot.arrival_window;
						console.log(flex,' <--');
                        if( flex.length != 0 ){
                            //flex.forEach(function(element) {
								//console.log('in the loop');
							
                                var newSpot = {
                                    "hours" : spot.hours,
                                    "minutes" : spot.minutes,
                                    "service_date" : service_date,
                                    //"value" : element.value,
                                    "remaining" : spot.free
                                };
								
								//console.log("New Spot: ",newSpot);

                                var hours = (newSpot.hours < 10) ? '0' + newSpot.hours : newSpot.hours;
                                var minutes = (newSpot.minutes < 10) ? '0' + newSpot.minutes : newSpot.minutes;
                                var sDate = newSpot.service_date + "T" + hours + ":" + minutes;

                                //console.log("service_date: ",sDate);

                                newSpot.format_date = sDate;
                                newSpot.time = $filter('date')(sDate, 'h:mma');
                                newSpot.pretty_date = $filter('date')(sDate, 'MM/dd/yyyy @ h:mma');

                                //var flex_hours = spot.hours;
                                var end_hours = parseInt(hours) + parseInt(flex);
                                end_hours = (end_hours < 10) ? '0' + end_hours : end_hours;
                                var endDate = newSpot.service_date + "T" + end_hours + ":00";

                                newSpot.arrival_window = flex;
                                newSpot.end_time = $filter('date')(endDate, 'h:mma');
								
								// Append the slot to the list only if it's available
								if(newSpot.remaining){
									//console.log('pushed', newSpot);
									newSpots.push(newSpot);
								}
                                
                            //});
                        } else {
                            var newSpot = spot;

                            var hours = (newSpot.hours < 10) ? '0' + newSpot.hours : newSpot.hours;
                            var minutes = (newSpot.minutes < 10) ? '0' + newSpot.minutes : newSpot.minutes;
                            var sDate = newSpot.service_date + "T" + hours + ":" + minutes;

                            newSpot.format_date = sDate;
                            newSpot.time = $filter('date')(sDate, 'h:mma');
                            newSpot.pretty_date = $filter('date')(sDate, 'MM/dd/yyyy @ h:mma');

                            newSpots.push(newSpot);
							//console.log("New Spots: ",newSpots);
                        }
                    });

                    if (!service.airnbnBooking) {
                        newSpots = newSpots.filter(function (spot) {
                            // Exclude Time Range for Residential Users (from 11am to 03pm)
                            return (spot.hours < service.residentialSkipTime.from.hours)
                                || (spot.hours == service.residentialSkipTime.from.hours && spot.minutes <= service.residentialSkipTime.from.minutes)
                                || (spot.hours == service.residentialSkipTime.to.hours && spot.minutes >= service.residentialSkipTime.to.minutes)
                                || (spot.hours > service.residentialSkipTime.to.hours);
                        })
                    }

                    return newSpots;
                });
        }

        function getCustomFields() {
            return $http.get(apiUrl + '/booking/custom_fields', apiConfig)
                .then(function (results) {
                    //return results.data.custom_fields;
					var customFields = results.data;
                    //console.log("Custom Fields: ",customFields);
					return customFields;
                })
                .then(function (customFields) {
                    customFields = customFields.filter(function (customField) {
                        return customFieldsIds.indexOf(customField.id) > -1;
                    })

                    var customFieldsList = {

                        /* Sandbox
                        source: customFields[0],
                        hasPets: customFields[1],
                        petType: customFields[2],
                        checkout_time: customFields[3],
                        checkin_time: customFields[4],
                        turnovers: customFields[6],
                        unit_type: customFields[7]
                        */

                        
                        // Production
                        source: customFields[5],
                        hasPets: customFields[6],
                        petType: customFields[7],
                        checkout_time: customFields[3],
                        checkin_time: customFields[4],
                        turnovers: customFields[2],
                        unit_type: customFields[0]
                        
                    }

                    //console.log("customFieldsList", customFieldsList);

                    return customFieldsList;
                })
        }

        function getBookingEstimate(bookingInfo) {
            //Solves empty extras list
            bookingInfo[3].extras = bookingInfo[3].extras || [];
            bookingInfo[3].maids = bookingInfo[3].maids || 0;
            bookingInfo[3].minutes = bookingInfo[3].hours * 60 || 0;

            return $http.post(apiUrl + '/booking/estimate_price', getBookingEstimateRequest(bookingInfo), apiConfig)
                .then(function (results) {
                    //return results.data.estimate;
					var estimate = results.data;
                    //console.log("Estimate: ",estimate);
					return estimate;
                })
        }

        function createBooking(bookingInfo) {
            console.log('-------------- first occurance');
            return createPaymentToken(bookingInfo[6]).then(function (stripToken) {
                return sendBookingInfo(bookingInfo, stripToken);
            });
        }

        function createPaymentToken(paymentInfo) {
            var deferred = $q.defer();

            Stripe.card.createToken({
                number: paymentInfo.creditcard_number,
                cvc: paymentInfo.card_cvc,
                exp: paymentInfo.expiration_date
            }, function (status, response) {
                if (response.error) return deferred.reject(response.error);

                deferred.resolve(response.id);
            });

            return deferred.promise;
        }

        function sendBookingInfo(bookingInfo, stripeToken) {
            console.log('-------------- second occurance');
            return $http.post(apiUrl + '/booking', getBookingRequest(bookingInfo, stripeToken), apiConfig)
                .then(function (response) {
                    return response;
                })
        }

        function getBookingRequest(bookingInfo, stripeToken) {
            return BookingRequestService.getBookingRequest(bookingInfo, service.airnbnBooking, customFieldNames, customFieldsIds, stripeToken, {
                noLaundryDiscount: service.noLaundryDiscount,
                noLaundryDiscountCode: service.noLaundryDiscountCode
            });
        }

        function getBookingEstimateRequest(bookingInfo, stripeToken) {
            return BookingRequestService.getBookingEstimateRequest(bookingInfo, service.airnbnBooking, stripeToken, {
                noLaundryDiscount: service.noLaundryDiscount,
                noLaundryDiscountCode: service.noLaundryDiscountCode
            });
        }
    }
})();