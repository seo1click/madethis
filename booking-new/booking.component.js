(function () {
    'use strict';

    // angular.module('app', [
    //   '720kb.tooltips'
    //  ]);
    
    angular
        .module('booking-new', ['ui.materialize', 'angularPayments', 'logglyLogger'])
        .config(function (LogglyLoggerProvider) {
            // LogglyLoggerProvider.inputToken('0b576631-9226-4628-9144-5bcb5198f6d9').sendConsoleErrors(true).logToConsole(true);
        })
        .run(function ($log) {
            $log.info('Maidthis Loggly Test');
        })
        .component('booking', {
            templateUrl: 'booking-new/booking.template.html',
            controller: BookingController,
            bindings: {
                airnbn: '<'
            }
        });


    BookingController.inject = ['$scope', 'BookingApi', '$log', '$window'];
    function BookingController($scope, BookingApi, $log, $window) {
        var $ctrl = this;

        $ctrl.currentStep;
        $ctrl.totalSteps;
        $ctrl.formStep;
        $ctrl.successPayment = false;
        $ctrl.savingBooking = false;

        $ctrl.goNext = goNext;
        $ctrl.goBack = goBack;
        $ctrl.bookNow = bookNow;

        $ctrl.lists = {
            services: []
        }
        $ctrl.steps = {
            1: {},
            2: {},
            3: {},
            4: {},
            5: {},
            6: {},
            7: {},
            8: {},
            9: {},
            10: {},
            11: {},
            12: {},
            13: {}
        }
        $ctrl.listsLoaded = false;

        ////////////////

        $ctrl.$onInit = function () {
            $ctrl.airnbn = BookingApi.airnbnBooking = ($ctrl.airnbn && $ctrl.airnbn !== 'false') ? true : false;

            /** Initial State of the Steps */
            $ctrl.currentStep = 1;
            $ctrl.totalSteps = 13;

            /** Get Lists from API */
            BookingApi.getLists().then(function (results) {
                $ctrl.lists = results;
                $ctrl.listsLoaded = true;
            }).catch(function (error) {
                $log.debug(error);
                Materialize.toast("Server Error. Please try again later.", 10000, 'red'); 
            });
        };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestory = function () { };

        /** Listen for Estimate Request Event */
        $scope.$on('booking-form: get-estimate', getBookingEstimate);

        ////////////////

        function goNext() {
            if ($ctrl.currentStep + 1 <= $ctrl.totalSteps) {
                if ($ctrl.currentStep === 1 && !$ctrl.steps[3].service.hourly){
                    $ctrl.currentStep+=2;
                } else {
                    $ctrl.currentStep++;
                }
            }
        }

        function goBack() {
            if ($ctrl.currentStep - 1 !== 0) {
                if ($ctrl.currentStep === 3 && !$ctrl.steps[3].service.hourly){
                    $ctrl.currentStep-=2;
                } else {
                    $ctrl.currentStep--;
                }
            }
        }

        function bookNow() {
            $ctrl.savingBooking = true;
            $scope.$broadcast('booking-component: loading');
            BookingApi.createBooking($ctrl.steps).then(function () {
                $scope.$broadcast('booking-component: done-saving');
                $ctrl.successPayment = true;
                $ctrl.savingBooking = false;
                //$window.location = (BookingApi.airnbnBooking) ? BookingApi.airnbnRedirect : residentialRedirect;
                $window.location = (BookingApi.airnbnBooking) ? BookingApi.airnbnRedirect : "/thanks-for-booking";
            }).catch(function (error) {
                $scope.$broadcast('booking-component: done-saving');
                $log.debug(error);
                $ctrl.savingBooking = false;
		var testItem;
		var errorObject = error.data;
		for (var prop in errorObject) {
    			testItem = errorObject[prop];
    			break;
		}
		var errorMessage = testItem[0].message;
                Materialize.toast(errorMessage, 10000, 'red');
            })
        }

        function getBookingEstimate(e, bookingFormInfo) {
            $scope.$broadcast('booking-component: loading');

            BookingApi.getBookingEstimate(bookingFormInfo).then(function (estimate) {
                $scope.$broadcast('booking-component: set-estimate', estimate, {
                    service: bookingFormInfo[3].service.name,
                    date: bookingFormInfo[4].spot.pretty_date,
                    frequency: bookingFormInfo[4].frequency.name,
                    extras: bookingFormInfo[3].extras
                        .map(function(extra) {
                            var extraInfo = bookingFormInfo[3].service.extras.filter(function(_extra) {
                                return _extra.id === extra.id;
                            })[0];

                            extraInfo.quantity = extra.quantity;
                            extraInfo.icon = BookingApi.extrasIcons[extra.id];
                            return extraInfo;
                        })
                });
            }).catch(function (error) {
                $log.debug(error);
                Materialize.toast("Server Error getting Estimate. Please try again later.", 10000, 'red');
            })
        }
    };

})();