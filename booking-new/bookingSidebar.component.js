(function () {
    'use strict';

    angular
        .module('booking-new')
        .component('bookingSidebar', {
            templateUrl: 'booking-new/bookingSidebar.template.html',
            controller: BookingSidebarController,
            bindings: {
                currentStep: '<',
                airnbn: "="
            },
        });

    BookingSidebarController.inject = ['$scope'];
    function BookingSidebarController($scope) {
        var $ctrl = this;
        $ctrl.loadingEstimate = false;
        $ctrl.estimateInfo;
        $ctrl.bookingInfo;

        ////////////////

        $ctrl.$onInit = function () { };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestory = function () { };

        $scope.$on('booking-component: loading', function () { $ctrl.loadingEstimate = true; });

        $scope.$on('booking-component: set-estimate', setEstimateInfo);

        $scope.$on('booking-component: done-saving', function () { $ctrl.loadingEstimate = false; });

        ////////////////

        function setEstimateInfo(e, estimateInfo, bookingInfo) {
            $ctrl.estimateInfo = estimateInfo;
            $ctrl.bookingInfo = bookingInfo;
            $ctrl.loadingEstimate = false;
        }

    }
})();